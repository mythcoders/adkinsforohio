//= require rails-ujs
//= require jquery/dist/jquery.min
//= require bootstrap/dist/js/bootstrap
//= require typed.js/lib/typed.min
//= require moment/min/moment.min
//= require tempusdominus-core/build/js/tempusdominus-core
//= require tempusdominus-bootstrap-4/src/js/tempusdominus-bootstrap-4
//= require select2/dist/js/select2.min
//= require select2

$(function () {
  $.fn.datetimepicker.Constructor.Default = $.extend(
    {},
    $.fn.datetimepicker.Constructor.Default,
    {
      icons: {
        time: "fad fa-clock",
        date: "fad fa-calendar-alt",
        up: "fad fa-long-arrow-up",
        down: "fad fa-long-arrow-down",
        previous: "fad fa-long-arrow-left",
        next: "fad fa-long-arrow-right",
        today: "fad fa-calendar-day",
        clear: "fad fa-trash-alt",
        close: "fad fa-times"
      }
    }
  );
  $("[data-date-time-picker]").datetimepicker({
    format: "MM/DD/YYYY hh:mm A",
    toolbarPlacement: 'bottom',
    buttons: {
      showToday: true,
      showClear: true,
      showClose: true
    }
  });
  $("[data-date-picker]").datetimepicker({
    format: "MM/DD/YYYY",
    toolbarPlacement: 'bottom',
    buttons: {
      showToday: true,
      showClear: true,
      showClose: true
    }
  });
});