# frozen_string_literal: true

class Expense < ApplicationRecord
  has_paper_trail
  belongs_to :contact
  belongs_to :event, required: false
  belongs_to :type, class_name: 'ExpenseType', foreign_key: 'expense_type_id'

  scope :recent, -> { where('created_at >= ? AND deleted_at IS NULL', Time.current - 1.month) }
end
