# frozen_string_literal: true

class User < ApplicationRecord
  has_paper_trail ignore: %i[current_sign_in_at last_sign_in_at sign_in_count
                             last_sign_in_ip current_sign_in_ip failed_attempts
                             encrypted_password reset_password_token confirmation_token]
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
         :validatable, :confirmable, :lockable, :timeoutable

  validate :email_domain_is_adkinsforohio

  def email_domain
    email.split('@').last
  end

  def name
    NameFormatter.display first_name, nil, last_name
  end

  private

  def email_domain_is_adkinsforohio
    if email_domain != 'adkinsforohio.com'
      Raven.capture_message(
        'Signup for non-adkinsforohio account',
        level: 'warning'
      )
      errors.add(:email, 'Domain not valid')
    end
  end
end
