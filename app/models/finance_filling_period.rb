# frozen_string_literal: true

class FinanceFillingPeriod < ApplicationRecord
  scope :current, -> { where('start_date <= :now AND end_date >= :now', {now: Time.current}).take(1) }
end
