# frozen_string_literal: true

class VolunteerDuty < ApplicationRecord
  has_paper_trail
  has_many :contact_volunteer_duties
  has_many :contacts, through: :contact_volunteer_duties

  default_scope { order(:name) }
  scope :active, -> { where(active: true) }
end
