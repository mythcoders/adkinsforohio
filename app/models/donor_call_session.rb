# frozen_string_literal: true

class DonorCallSession < ApplicationRecord
  has_paper_trail
  has_many :donor_calls
  has_many :contacts, through: :donor_calls
end
