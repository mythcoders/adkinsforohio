# frozen_string_literal: true

class ContactVolunteerDuty < ApplicationRecord
  belongs_to :contact
  belongs_to :volunteer_duty
end
