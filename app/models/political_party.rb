# frozen_string_literal: true

class PoliticalParty < ApplicationRecord
  has_paper_trail
  has_many :voters

  default_scope { order(:name) }
  scope :active, -> { where(active: true) }
end
