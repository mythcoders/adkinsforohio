# frozen_string_literal: true

class Contact < ApplicationRecord
  has_paper_trail
  has_many :donor_calls
  has_many :contributions
  has_many :expenses
  has_many :contact_volunteer_duties, dependent: :destroy
  has_many :volunteer_duties, through: :contact_volunteer_duties
  belongs_to :political_party, required: false

  enum source: { manually: 0, organic: 1, imported: 2 }
  scope :recent, -> { where('created_at >= ?', Time.current - 1.month) }

  accepts_nested_attributes_for :volunteer_duties
  validates_presence_of :name

  def volunteer?
    contact_volunteer_duties.any?
  end

  def donor?
    contributions.any?
  end

  def name
    @name ||= if organization_name.present?
                organization_name
              else
                NameFormatter.display first_name, middle_name, last_name
              end
  end

  def full_address
    @full_address ||= AddressFormatter.display address, nil, city, state, zip_code
  end
end
