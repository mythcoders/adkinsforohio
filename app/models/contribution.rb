# frozen_string_literal: true

class Contribution < ApplicationRecord
  has_paper_trail
  belongs_to :form, class_name: 'ContributionForm', foreign_key: 'contribution_form_id', required: false
  belongs_to :type, class_name: 'ContributionType', foreign_key: 'contribution_type_id'
  belongs_to :contact
  belongs_to :event, required: false

  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :recent, -> { not_deleted.where('created_at >= ?', Time.current - 1.month) }
end
