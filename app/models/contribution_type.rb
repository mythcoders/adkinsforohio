# frozen_string_literal: true

class ContributionType < ApplicationRecord
  has_paper_trail
  has_many :contributions

  default_scope { order(:name) }
  scope :active, -> { where(active: true) }
end
