# frozen_string_literal: true

class DonorCall < ApplicationRecord
  OUTCOMES = %i[left_message talked_to do_not_call].freeze

  has_paper_trail
  belongs_to :donor_call_session
  belongs_to :contact
end
