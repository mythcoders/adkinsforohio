# frozen_string_literal: true

class Event < ApplicationRecord
  has_paper_trail
  has_many :contributions

  scope :fundraisers, -> { where(fundraiser: true) }
end
