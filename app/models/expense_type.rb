# frozen_string_literal: true

class ExpenseType < ApplicationRecord
  has_paper_trail
  has_many :expenses

  default_scope { order(:name) }
  scope :active, -> { where(active: true) }
end
