# frozen_string_literal: true

class VolunteerController < ApplicationController
  def index
    @contact = Contact.new
    @duties = VolunteerDuty.active.order(name: :asc)
  end

  def create
    @contact = Contact.new(contact_params)
    recaptcha_valid = verify_recaptcha(model: @contact, action: 'volunteer')
    @contact.organic!
    if recaptcha_valid && @contact.save
      flash[:success] = t('volunteer_thanks')
      # TODO: Send an email
    else
      flash[:error] = t('create.failure')
    end
    redirect_back(fallback_location: root_path)
  end

  private

  def contact_params
    params.require(:contact).permit(:first_name, :last_name, :email, :phone_number, :zip_code,
                                    volunteer_duty_ids: [])
  end
end
