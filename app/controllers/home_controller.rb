# frozen_string_literal: true

class HomeController < ApplicationController
  layout 'landing', only: %i[index contact]

  def index
    @contact = Contact.new
  end
end
