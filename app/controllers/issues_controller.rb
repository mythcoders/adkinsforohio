# frozen_string_literal: true

class IssuesController < ApplicationController
  layout 'landing', except: %i[index]
end
