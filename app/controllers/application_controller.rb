# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_by_resource

  before_action :user_for_paper_trail
  before_action :raven_context
  before_action :configure_permitted_parameters, if: :devise_controller?

  def raven_context
    return unless user_signed_in?

    Raven.user_context(
      id: current_user.id,
      email: current_user.email,
      ip_address: request.ip
    )
  end

  def convert_datetime(value)
    return nil if value.blank?

    Time.zone.strptime(value, '%m/%d/%Y %I:%M %p')
  end

  def convert_date(value)
    return nil if value.blank?

    Date.strptime(value, '%m/%d/%Y')
  end

  private

  def configure_permitted_parameters
    additional_fields = %i[first_name last_name].freeze
    devise_parameter_sanitizer.permit(:sign_up, keys: additional_fields)
    devise_parameter_sanitizer.permit(:account_update, keys: additional_fields)
  end

  def layout_by_resource
    'devise' if devise_controller?
  end

  def user_for_paper_trail
    user_signed_in? ? current_user.id : 'Public user'
  end
end
