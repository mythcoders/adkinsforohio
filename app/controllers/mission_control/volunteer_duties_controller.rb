# frozen_string_literal: true

module MissionControl
  class VolunteerDutiesController < MissionControlController
    def index
      @duties = VolunteerDuty.all
    end
  end
end
