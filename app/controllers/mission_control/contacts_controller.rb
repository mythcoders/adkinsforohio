# frozen_string_literal: true

module MissionControl
  class ContactsController < MissionControlController
    include ContactsHelper

    before_action :set_contact, only: %i[show edit update destroy]
    before_action :set_parties, only: %i[new edit]

    def index
      @contacts = Contact.all.sort_by(&:name)
    end

    def new
      @contact = Contact.new
    end

    def create
      @contact = Contact.new contact_params
      @contact.manually!
      if @contact.save
        flash[:success] = t('create.success')
        redirect_to mc_contact_path @contact
      else
        flash[:error] = t('create.failure')
        set_parties
        render 'new'
      end
    end

    def update
      if @contact.update(contact_params)
        flash[:success] = t('update.success')
        redirect_to mc_contact_path @contact
      else
        flash[:error] = t('update.failure')
        set_parties
        render 'edit'
      end
    end

    def destroy
      if @contact.delete
        flash[:success] = t('destroy.success')
        redirect_to mc_contacts_path
      else
        flash[:error] = t('destroy.failure')
        redirect_to mc_contact_path @contact
      end
    end

    private

    def contact_params
      params.require(:contact).permit(:id, :first_name, :middle_name, :last_name, :organization_name,
                                      :address, :city, :state, :zip_code, :phone_number, :email, :employer,
                                      :birth_year, :voter_id, :voter_status, :precinct, :house, :senate,
                                      :congressional, :village, :township, :school, :political_party_id,
                                      volunteer_duty_ids: [])
    end

    def set_contact
      @contact = Contact.find params[:id]
    end

    def set_parties
      @parties = PoliticalParty.active.order(:name)
    end
  end
end
