# frozen_string_literal: true

module MissionControl
  class DashboardController < MissionControlController
    def index
      @recent_contacts = Contact.recent.organic.count
    end
  end
end
