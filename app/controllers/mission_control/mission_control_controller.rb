# frozen_string_literal: true

module MissionControl
  class MissionControlController < ApplicationController
    before_action :authenticate_user!
    layout 'mission_control'
  end
end
