# frozen_string_literal: true

module MissionControl
  class ExpensesController < MissionControlController
    before_action :set_expense, only: %i[show edit update destroy]
    before_action :set_dropdown_values, only: %i[new edit]

    def index
      @expenses = Expense.all
    end

    def new
      @expense = Expense.new(contact_id: params[:contact_id])
    end

    def create
      @expense = Expense.new expense_params
      if @expense.save
        flash[:success] = t('create.success')
        redirect_to edit_mc_expense_path @expense
      else
        flash[:error] = t('create.failure')
        set_dropdown_values
        render 'new'
      end
    end

    def update
      if @expense.update(expense_params)
        flash[:success] = t('update.success')
        redirect_to edit_mc_expense_path @expense
      else
        flash[:error] = t('update.failure')
        set_dropdown_values
        render 'edit'
      end
    end

    def destroy
      @contribution.delete_at = Time.current
      if @expense.save
        flash[:success] = t('destroy.success')
        redirect_to mc_expenses_path
      else
        flash[:error] = t('destroy.failure')
        redirect_to mc_expense_path @expense
      end
    end

    private

    def expense_params
      parameters = params.require(:expense)
                          .permit(:id, :contact_id, :expense_date, :amount, :description,
                                  :expense_type_id, :event_id, :deleted_at)
      parameters[:expense_date] = convert_date(parameters[:expense_date])
      parameters[:deleted_at] = convert_datetime(parameters[:deleted_at])
      parameters
    end

    def set_expense
      @expense = Expense.find params[:id]
    end

    def set_dropdown_values
      @contacts = Contact.all.sort_by(&:name)
      @types = ExpenseType.active
      @events = Event.fundraisers
    end
  end
end
