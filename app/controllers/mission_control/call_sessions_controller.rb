# frozen_string_literal: true

module MissionControl
  class CallSessionsController < MissionControlController
    def index; end
  end
end
