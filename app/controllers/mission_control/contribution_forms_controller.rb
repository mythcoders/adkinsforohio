# frozen_string_literal: true

module MissionControl
  class ContributionFormsController < MissionControlController
    def index
      @forms = ContributionForm.all
    end
  end
end
