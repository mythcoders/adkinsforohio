# frozen_string_literal: true

module MissionControl
  class PoliticalPartiesController < MissionControlController
    def index
      @parties = PoliticalParty.all
    end
  end
end
