# frozen_string_literal: true

module MissionControl
  class ExpenseTypesController < MissionControlController
    def index
      @types = ExpenseType.all
    end
  end
end
