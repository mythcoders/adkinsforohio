# frozen_string_literal: true

module MissionControl
  class EventsController < MissionControlController
    before_action :set_event, only: %i[show edit update destroy]

    def index
      @events = Event.all
    end

    def new
      @event = Event.new
    end

    def create
      @event = Event.new event_params
      if @event.save
        flash[:success] = t('create.success')
        redirect_to mc_event_path @event
      else
        flash[:error] = t('create.failure')
        render 'new'
      end
    end

    def update
      if @event.update(event_params)
        flash[:success] = t('update.success')
        redirect_to mc_event_path @event
      else
        flash[:error] = t('update.failure')
        render 'edit'
      end
    end

    def destroy
      if @event.delete
        flash[:success] = t('destroy.success')
        redirect_to mc_events_path
      else
        flash[:error] = t('destroy.failure')
        redirect_to mc_event_path @event
      end
    end

    private

    def event_params
      parameters = params.require(:event).permit(:id, :name, :start, :end, :description, :rsvp_link, :fundraiser)
      parameters[:start] = convert_datetime(parameters[:start])
      parameters[:end] = convert_datetime(parameters[:end])
      parameters
    end

    def set_event
      @event = Event.find params[:id]
    end
  end
end
