# frozen_string_literal: true

module MissionControl
  class ContributionsController < MissionControlController
    before_action :set_contribution, only: %i[show edit update destroy]
    before_action :set_dropdown_values, only: %i[new edit]

    def index
      @contributions = Contribution.all
    end

    def new
      @contribution = Contribution.new(contact_id: params[:contact_id])
    end

    def create
      @contribution = Contribution.new contribution_params
      if @contribution.save
        flash[:success] = t('create.success')
        redirect_to edit_mc_contribution_path @contribution
      else
        flash[:error] = t('create.failure')
        set_dropdown_values
        render 'new'
      end
    end

    def update
      if @contribution.update(contribution_params)
        flash[:success] = t('update.success')
        redirect_to edit_mc_contribution_path @contribution
      else
        flash[:error] = t('update.failure')
        set_dropdown_values
        render 'edit'
      end
    end

    def destroy
      @contribution.delete_at = Time.current
      if @contribution.save
        flash[:success] = t('destroy.success')
        redirect_to mc_contributions_path
      else
        flash[:error] = t('destroy.failure')
        redirect_to mc_contribution_path @contribution
      end
    end

    private

    def contribution_params
      parameters = params.require(:contribution)
                          .permit(:id, :contact_id, :contribution_date, :amount, :description, :thanked,
                                  :contribution_type_id, :contribution_form_id, :event_id, :deleted_at)
      parameters[:contribution_date] = convert_date(parameters[:contribution_date])
      parameters[:deleted_at] = convert_datetime(parameters[:deleted_at])
      parameters
    end

    def set_contribution
      @contribution = Contribution.find params[:id]
    end

    def set_dropdown_values
      @contacts = Contact.all.sort_by(&:name)
      @types = ContributionType.active
      @forms = ContributionForm.active
      @events = Event.fundraisers
    end
  end
end
