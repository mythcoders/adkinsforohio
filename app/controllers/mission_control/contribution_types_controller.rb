# frozen_string_literal: true

module MissionControl
  class ContributionTypesController < MissionControlController
    def index
      @types = ContributionType.all
    end
  end
end
