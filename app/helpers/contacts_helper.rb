# frozen_string_literal: true

module ContactsHelper
  def political_party_icon(party)
    party.icon.present? ? party.icon : 'flag-usa'
  end
end
