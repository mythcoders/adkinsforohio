class AddSourceToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :source, :integer, default: 0
  end
end
