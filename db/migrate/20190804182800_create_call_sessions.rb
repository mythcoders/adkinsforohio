class CreateCallSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :donor_call_sessions do |t|
      t.datetime :start
      t.datetime :end
      t.datetime :started_at
      t.integer :time_spent_in_seconds
      t.decimal :goal_in_usd
      t.belongs_to :caller, index: true, foreign_key: {to_table: :users}
      t.timestamps
    end

    create_table :donor_calls do |t|
      t.belongs_to :donor_call_session, index: true, null: false, foreign_key: true
      t.belongs_to :contact, index: true, null: false, foreign_key: true
      t.datetime :called_at
      t.string :outcome
      t.string :notes
      t.decimal :amount_pledged
      t.boolean :email_follow_up
      t.timestamps
    end

    create_table :contribution_forms do |t|
      t.string :name
      t.string :reporting_code, limit: 5
      t.boolean :active
      t.timestamps
    end

    create_table :contribution_types do |t|
      t.string :name
      t.string :reporting_code, limit: 5
      t.boolean :active
      t.timestamps
    end

    create_table :contributions do |t|
      t.belongs_to :contact, index: true, null: false, foreign_key: true
      t.date :contribution_date
      t.decimal :amount
      t.string :description
      t.belongs_to :contribution_type, index: true, null: false, foreign_key: true
      t.belongs_to :contribution_form, index: true, null: false, foreign_key: true
      t.belongs_to :event, index: true, null: true, foreign_key: true
      t.timestamps
    end

    create_table :expense_types do |t|
      t.string :name
      t.string :reporting_code, limit: 5
      t.boolean :active
      t.timestamps
    end

    create_table :expenses do |t|
      t.belongs_to :contact, index: true, null: false, foreign_key: true
      t.belongs_to :expense_type, index: true, null: false, foreign_key: true
      t.date :expense_date
      t.decimal :amount
      t.string :description
      t.timestamps
    end
  end
end
