class AllowNullContributionForms < ActiveRecord::Migration[5.2]
  def change
    change_column_null :contributions, :contribution_form_id, true
  end
end
