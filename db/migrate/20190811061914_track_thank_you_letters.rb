class TrackThankYouLetters < ActiveRecord::Migration[5.2]
  def change
    add_column :contributions, :thanked, :boolean, default: false
  end
end
