class CreateFinanceFillingPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :finance_filling_periods do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.date :due_date
      t.timestamps
    end
  end
end
