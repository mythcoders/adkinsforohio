class AddEventToExpenses < ActiveRecord::Migration[5.2]
  def change
    add_reference :expenses, :event, foreign_key: true, index: true, null: true
  end
end
