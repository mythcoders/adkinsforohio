class CreateContact < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email,              null: false, default: ""
      t.string :first_name
      t.string :last_name
      t.string :encrypted_password, null: false, default: ""
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
      t.datetime :remember_created_at
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.integer  :failed_attempts, default: 0, null: false
      t.datetime :locked_at
      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true

    create_table :political_parties do |t|
      t.string :name
      t.string :reporting_code, limit: 5
      t.string :icon
      t.boolean :active
      t.timestamps
    end

    create_table :volunteer_duties do |t|
      t.string :name
      t.boolean :active
      t.timestamps
    end

    create_table :contacts do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :organization_name
      t.string :address
      t.string :city
      t.string :state, limit: 2
      t.string :zip_code, limit: 9
      t.string :phone_number
      t.string :email
      t.string :employer
      t.integer :birth_year
      t.string :voter_id
      t.string :voter_status
      t.string :precinct
      t.string :house, limit: 2
      t.string :senate, limit: 2
      t.string :congressional, limit: 2
      t.string :village
      t.string :township
      t.string :school
      t.belongs_to :political_party, index: true, null: true, foreign_key: true
      t.timestamps
    end

    create_table :contact_volunteer_duties do |t|
      t.belongs_to :contact, index: true, foreign_key: true
      t.belongs_to :volunteer_duty, index: true, foreign_key: true
      t.timestamps
    end
  end
end
