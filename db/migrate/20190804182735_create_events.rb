class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.datetime :start
      t.datetime :end
      t.string :rsvp_link
      t.decimal :fundraiser
      t.timestamps
    end
  end
end
