class AlterEventType < ActiveRecord::Migration[5.2]
  def change
    remove_column :events, :fundraiser, :decimal
    add_column :events, :fundraiser, :boolean
  end
end
