class AddDeleteDateForFinances < ActiveRecord::Migration[5.2]
  def change
    add_column :expenses, :deleted_at, :datetime
    add_column :contributions, :deleted_at, :datetime
    add_column :finance_filling_periods, :amount_paid, :decimal
  end
end
