# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_11_061914) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contact_volunteer_duties", force: :cascade do |t|
    t.bigint "contact_id"
    t.bigint "volunteer_duty_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_contact_volunteer_duties_on_contact_id"
    t.index ["volunteer_duty_id"], name: "index_contact_volunteer_duties_on_volunteer_duty_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.string "organization_name"
    t.string "address"
    t.string "city"
    t.string "state", limit: 2
    t.string "zip_code", limit: 9
    t.string "phone_number"
    t.string "email"
    t.string "employer"
    t.integer "birth_year"
    t.string "voter_id"
    t.string "voter_status"
    t.string "precinct"
    t.string "house", limit: 2
    t.string "senate", limit: 2
    t.string "congressional", limit: 2
    t.string "village"
    t.string "township"
    t.string "school"
    t.bigint "political_party_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "source", default: 0
    t.index ["political_party_id"], name: "index_contacts_on_political_party_id"
  end

  create_table "contribution_forms", force: :cascade do |t|
    t.string "name"
    t.string "reporting_code", limit: 5
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contribution_types", force: :cascade do |t|
    t.string "name"
    t.string "reporting_code", limit: 5
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contributions", force: :cascade do |t|
    t.bigint "contact_id", null: false
    t.date "contribution_date"
    t.decimal "amount"
    t.string "description"
    t.bigint "contribution_type_id", null: false
    t.bigint "contribution_form_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.boolean "thanked", default: false
    t.index ["contact_id"], name: "index_contributions_on_contact_id"
    t.index ["contribution_form_id"], name: "index_contributions_on_contribution_form_id"
    t.index ["contribution_type_id"], name: "index_contributions_on_contribution_type_id"
    t.index ["event_id"], name: "index_contributions_on_event_id"
  end

  create_table "donor_call_sessions", force: :cascade do |t|
    t.datetime "start"
    t.datetime "end"
    t.datetime "started_at"
    t.integer "time_spent_in_seconds"
    t.decimal "goal_in_usd"
    t.bigint "caller_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["caller_id"], name: "index_donor_call_sessions_on_caller_id"
  end

  create_table "donor_calls", force: :cascade do |t|
    t.bigint "donor_call_session_id", null: false
    t.bigint "contact_id", null: false
    t.datetime "called_at"
    t.string "outcome"
    t.string "notes"
    t.decimal "amount_pledged"
    t.boolean "email_follow_up"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_donor_calls_on_contact_id"
    t.index ["donor_call_session_id"], name: "index_donor_calls_on_donor_call_session_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "start"
    t.datetime "end"
    t.string "rsvp_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "fundraiser"
  end

  create_table "expense_types", force: :cascade do |t|
    t.string "name"
    t.string "reporting_code", limit: 5
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "expenses", force: :cascade do |t|
    t.bigint "contact_id", null: false
    t.bigint "expense_type_id", null: false
    t.date "expense_date"
    t.decimal "amount"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "event_id"
    t.datetime "deleted_at"
    t.index ["contact_id"], name: "index_expenses_on_contact_id"
    t.index ["event_id"], name: "index_expenses_on_event_id"
    t.index ["expense_type_id"], name: "index_expenses_on_expense_type_id"
  end

  create_table "finance_filling_periods", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.date "due_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "amount_paid"
  end

  create_table "political_parties", force: :cascade do |t|
    t.string "name"
    t.string "reporting_code", limit: 5
    t.string "icon"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer "failed_attempts", default: 0, null: false
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.jsonb "object"
    t.jsonb "object_changes"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "volunteer_duties", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "contact_volunteer_duties", "contacts"
  add_foreign_key "contact_volunteer_duties", "volunteer_duties"
  add_foreign_key "contacts", "political_parties"
  add_foreign_key "contributions", "contacts"
  add_foreign_key "contributions", "contribution_forms"
  add_foreign_key "contributions", "contribution_types"
  add_foreign_key "contributions", "events"
  add_foreign_key "donor_call_sessions", "users", column: "caller_id"
  add_foreign_key "donor_calls", "contacts"
  add_foreign_key "donor_calls", "donor_call_sessions"
  add_foreign_key "expenses", "contacts"
  add_foreign_key "expenses", "events"
  add_foreign_key "expenses", "expense_types"
end
