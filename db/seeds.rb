PoliticalParty.create!([
  { name: 'Constitution', reporting_code: 'C', active: true  },
  { name: 'Democrat', reporting_code: 'D', icon: 'democrat', active: true  },
  { name: 'Green', reporting_code: 'G', icon: 'dove', active: true  },
  { name: 'Libertarian', reporting_code: 'L', icon: 'balance-scale', active: true  },
  { name: 'Republican', reporting_code: 'R', icon: 'republican', active: true  },
  { name: 'Socialist', reporting_code: 'S', active: true  },
  { name: 'Unaffiliated', reporting_code: 'U', active: true  }
])

ContributionForm.create!([
  { name: 'Cash', reporting_code: '1', active: true },
  { name: 'Check/Money Order', reporting_code: '2', active: true },
  { name: 'Credit Card', reporting_code: '3', active: true },
  { name: 'Electronic Transfer', reporting_code: '4', active: true },
  { name: 'Payroll Deduction', reporting_code: '5', active: true }
])

ContributionType.create!([
  { name: 'Regular', reporting_code: '31A', active: true },
  { name: 'In-Kind', reporting_code: '31J1', active: true },
  { name: 'Regular at Event', reporting_code: '31E', active: true }
])

ExpenseType.create!([
  { name: 'Regular', reporting_code: '31B', active: true },
  { name: 'In-Kind', reporting_code: '31J2', active: true }
])

VolunteerDuty.create!([
  { name: 'Door knocking', active: true },
  { name: 'Call or text voters', active: true },
  { name: 'Host a house party', active: true },
  { name: 'Yard sign', active: true },
  { name: 'Help at an event', active: true }
])

FinanceFillingPeriod.create!([
  { name: '2019 Semiannual', start_date: '2019-01-01', end_date: '2019-06-30', due_date: '2019-07-31' },
  { name: '2019 Annual', start_date: '2019-07-01', end_date: '2019-12-31', due_date: '2020-01-31' }
])