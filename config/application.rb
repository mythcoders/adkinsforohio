# frozen_string_literal: true

require_relative 'boot'

# require "rails"
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
# require "active_storage/engine"
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
# require "action_cable/engine"
require 'sprockets/railtie'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Campaign
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.encoding = 'utf-8'
    config.time_zone = 'Eastern Time (US & Canada)'
    config.public_file_server.enabled
    config.autoload_paths += %W[#{config.root}/lib]
    config.eager_load_paths += %W[#{config.root}/lib]
    # config.require_master_key = true
    config.generators.system_tests = nil

    # Logging
    config.lograge.enabled = true
    config.lograge.custom_payload do |controller|
      {
        host: controller.request.host,
        user_id: controller.current_user.try(:id)
      }
    end
    config.lograge.custom_options = lambda do |event|
      exceptions = %w[controller action format id]
      {
        params: event.payload[:params].except(*exceptions)
      }
    end

    if Rails.application.credentials.email.present?
      # Email
      config.action_mailer.delivery_method = :smtp
      config.action_mailer.perform_deliveries = true
      config.action_mailer.raise_delivery_errors = true
      config.action_mailer.perform_caching = false
      ActionMailer::Base.smtp_settings = {
        user_name: Rails.application.credentials.email[:username],
        password: Rails.application.credentials.email[:password],
        domain: 'adkinsforohio.com',
        address: 'email-smtp.us-east-1.amazonaws.com',
        port: 587,
        authentication: :plain,
        enable_starttls_auto: true
      }
    end
  end
end
