# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.configure do
  config.cache_classes = false

  config.eager_load = false
  config.log_level = :debug

  config.consider_all_requests_local = true

  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control': "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false
    config.cache_store = :null_store
  end

  config.action_mailer.perform_caching = false
  config.action_mailer.default_url_options = {
    host: 'http://adkinsforohio.localhost'
  }

  # config.action_mailer.delivery_method = Hermes::RailsAdapter
  config.action_mailer.preview_path = "#{Rails.root}/spec/mailer_previews"

  config.active_support.deprecation = :log
  # config.active_storage.service = :local
  config.active_job.queue_adapter = :inline
  config.active_record.migration_error = :page_load

  config.assets.debug = false

  config.assets.quiet = true
  config.web_console.whiny_requests = false
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  logger           = ActiveSupport::Logger.new(STDOUT)
  logger.formatter = config.log_formatter
  config.logger = ActiveSupport::TaggedLogging.new(logger)
end
# rubocop:enable Metrics/BlockLength
