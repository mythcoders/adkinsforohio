# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  root to: 'home#index'

  # direct :donate do |form: 'adkinsforohio', ref: 'website', amount: nil, recurring: false|
  #   url = "https://secure.actblue.com/donate/#{form}?refcode=#{ref}"
  #   url += "&amount=#{amount}" unless amount.nil?
  #   url += '&recurring=1' if recurring
  #   url
  # end

  direct :facebook do
    'https://www.facebook.com/adkinsforohio'
  end

  direct :twitter do
    'https://www.twitter.com/adkinsforohio'
  end

  direct :instagram do
    'https://www.instagram.com/adkinsforohio'
  end

  direct :gitlab do
    'https://gitlab.com/mythcoders/adkinsforohio'
  end

  direct :youtube do
    'https://www.youtube.com/channel/UCc8anT7XR-VvBHfYX9XHS8g'
  end

  direct :reddit do
    'https://www.reddit.com/u/adkinsforohio'
  end

  get 'about', to: 'home#about'
  get 'contact', to: 'home#contact'
  get 'endorsements', to: 'home#endorsements'
  get 'events', to: 'home#events'
  get 'volunteer', to: 'volunteer#index'
  post 'volunteer', to: 'volunteer#create', as: 'new_volunteer'

  scope :news do
    root to: 'news#index', as: 'news'
    get 'sb208-abortion-misinformation', to: 'news#sb208', as: 'news_sb208'
    get 'suspension', to: 'news#suspension', as: 'news_suspension'
  end

  scope :issues do
    root to: 'issues#index', as: 'issues'
    get 'healthcare', to: 'issues#healthcare'
    get 'education', to: 'issues#education'
    get 'economy', to: 'issues#economy'
    get 'democracy', to: 'issues#democracy'
    get 'environment', to: 'issues#environment'
    get 'technology', to: 'issues#technology'
    get 'guns', to: 'issues#guns'
    get 'justice', to: 'issues#justice'
    get 'lgbtq', to: 'issues#lgbtq'
    get 'womens_rights', to: 'issues#womens_rights'
    get 'rural', to: 'issues#rural'
    get 'seniors', to: 'issues#seniors'
    get 'transportation', to: 'issues#transportation'
    get 'veterans', to: 'issues#veterans'
  end

  get '/missioncontrol' => redirect('/mc')
  get '/mission_control' => redirect('/mc')
  scope module: :mission_control, path: 'mc', as: 'mc' do
    root to: 'dashboard#index', as: 'dashboard'
    get 'finances', to: 'dashboard#finances', as: 'finances_dashboard'
    get 'about', to: 'dashboard#about', as: 'about'
    resources :call_sessions
    resources :contacts
    resources :contribution_forms
    resources :contribution_types
    resources :contributions
    resources :events
    resources :expense_types
    resources :expenses
    resources :political_parties
    resources :volunteer_duties
  end
end
