# frozen_string_literal: true

if Rails.application.credentials.unleash.present?
  Unleash.configure do |config|
    config.url = Rails.application.credentials.unleash[:url]
    config.instance_id = Rails.application.credentials.unleash[:instance_id]
    config.app_name = Rails.env
    config.logger   = Rails.logger
  end

  UNLEASH = Unleash::Client.new
end
