# frozen_string_literal: true

class NameFormatter
  FULL_NAME_FORMAT = [LAST_FIRST = 1, FIRST_LAST = 2].freeze

  class << self
    def display(first_name, middle_initial, last_name, suffix = nil, style = 2)
      return_value = nil

      if style == 1
        return_value = "#{last_name}, #{first_name}" if !last_name.blank? || !first_name.blank?

        unless middle_initial.blank?
          return_value += " #{middle_initial}"
          return_value += '.' if middle_initial.length == 1
        end

        unless suffix.blank?
          return_value += ", #{suffix}"

          return_value += '.' if suffix == 'Jr' || suffix == 'Sr'
        end
      else
        return_value = first_name.to_s

        unless middle_initial.blank?
          return_value += " #{middle_initial}"
          return_value += '.' if middle_initial.length == 1
        end

        return_value += " #{last_name}"

        unless suffix.blank?
          return_value += ", #{suffix}"

          return_value += '.' if suffix == 'Jr' || suffix == 'Sr'
        end
      end

      return_value
    end
  end
end
