# frozen_string_literal: true

class AddressFormatter
  ADDRESS_FORMAT = [WITH_LINE_BREAKS = 1, ONE_LINE = 2].freeze

  class << self
    # TODO: move custom format/display helpers into seperate classes and import them similar to Redmine, but check Gitlab
    def display(address_line_1, address_line_2, city, state, zip_code, zip_code_four = '', style = 1)
      return_value = ''
      delimiter = style == 2 ? ', ' : '<br/>'

      return_value += address_line_1 unless address_line_1.blank?

      unless address_line_2.blank?
        return_value += delimiter unless return_value.empty?
        return_value += address_line_2
      end

      unless city.blank? && state.blank? && zip_code.blank?
        return_value += delimiter unless return_value.empty?

        unless city.blank?
          return_value += city

          return_value += ', ' unless state.blank? && zip_code.blank?
        end

        return_value += state unless state.blank?

        unless zip_code.blank?
          return_value += ' ' unless state.nil?

          if zip_code.length == 9 && zip_code_four.blank?
            zip_code_four = zip_code[5..4]
            zip_code = zip_code[0..5]
          end

          return_value += zip_code

          return_value += "-#{zip_code_four}" unless zip_code_four.blank?
        end
      end

      return_value
    end

    def format_address_as_html_link(address_line_1, address_line_2, city, state, zip_code, zip_code_four = '', style = 1, label = nil)
      formatted_address = format_address(address_line_1, address_line_2, city, state, zip_code, zip_code_four, style)
      parameters = style == 2 ? formatted_address : format_address(address_line_1, address_line_2, city, state, zip_code, zip_code_four, 2)

      url_for_bing_maps(parameters, formatted_address, label)
    end

    private

    def url_for_bing_maps(parameters, formatted_address, label)
      if formatted_address.nil?
        "<a class=\"btn btn-link\" href=\"http://bing.com/maps?q=#{parameters}\" target=\"_blank\">#{formatted_address}</a>"
      else
        "<a class=\"btn btn-link\" href=\"http://bing.com/maps?q=#{parameters}\" target=\"_blank\">#{label}</a>"
      end
    end
  end
end
