FROM nginx:alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY redirect/default.conf /etc/nginx/conf.d/default.conf
